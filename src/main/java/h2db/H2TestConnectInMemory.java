package h2db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2TestConnectInMemory {
    public static void main(String[] args) {

        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:h2:mem",
                    "sa",
                    "1234");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
