package h2db;

import java.sql.*;

public class H2TestConnectSchema {
    public static void main(String[] args) {

        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:h2:./testDB2;INIT=RUNSCRIPT FROM 'classpath:schema.sql'",
                    "",
                    "");


            Statement statement = connection.createStatement();


            ResultSet rs = statement.executeQuery("SELECT * FROM myTable");
            while (rs.next()) {
                int id = rs.getInt("id");
                String test1 = rs.getString("test1");
                String test2 = rs.getString("test2");
                System.out.println("ID: " + id + ", Test1: " + test1 + ", Test2: " + test2);
            }


            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
