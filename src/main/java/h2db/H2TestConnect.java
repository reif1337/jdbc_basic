package h2db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2TestConnect {
    public static void main(String[] args) {

        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:h2:./testDB",
                    "",
                    "");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
