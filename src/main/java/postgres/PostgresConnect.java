package postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresConnect {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/test",
                    "postgres",
                    "admin");

            System.out.println("Connected!!");

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
