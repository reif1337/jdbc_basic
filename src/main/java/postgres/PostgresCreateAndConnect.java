package postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgresCreateAndConnect {
    static String databasename = "myFirstDB";

    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/",
                    "postgres",
                    "admin");

            System.out.println("Connected");


            // CREATE DATABASE
            System.out.println("Creating DATABASE ...");
            Statement statement = connection.createStatement();
            String query = String.format("CREATE DATABASE %s", databasename);
            statement.executeUpdate(query);

            //statement.executeUpdate("""
            //    CREATE DATABASE databasename;
            //""");
            // ------------------------------------

            statement.close();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
