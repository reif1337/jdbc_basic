DROP TABLE IF EXISTS myTable;

create table myTable (
    id identity, /* postgres: serial, msqsql: auto_increment */
    test1 varchar(255),
    test2 varchar(255)
);

INSERT INTO myTable (test1, test2) VALUES
                                       ('Franz', 'Reichel'),
                                       ('Hans', 'Huber'),
                                       ('Kanns', 'Horstl');